import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.DefaultListModel;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.JSplitPane;
import javax.swing.JList;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map.Entry;
import java.awt.event.ActionEvent;
import javax.swing.ListModel;
import javax.swing.JFormattedTextField;
import java.awt.Color;

public class TagWindow extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8095939352095211688L;
	private final JPanel contentPanel = new JPanel();
	
	private JList<String> listImgTags;
	private JList<String> listAllTags;
	private JFormattedTextField textNewTag;
	
	private ArrayList<String> imgTags;
	private HashMap<String, Integer> allTags;
	
	@SuppressWarnings("unused")
	private SaveMeta sMeta;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			TagWindow dialog = new TagWindow(null, null, null);
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public TagWindow(ArrayList<String> imgTags, HashMap<String, Integer> allTags, SaveMeta sMeta) {
		this.imgTags = imgTags;
		this.allTags = allTags;
		this.sMeta = sMeta;
		setBounds(100, 100, 476, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		{
			JSplitPane splitPane = new JSplitPane();
			splitPane.setResizeWeight(.5d);
			contentPanel.add(splitPane, BorderLayout.CENTER);
			{
				listImgTags = new JList<String>();
				listImgTags.setSelectionBackground(new Color(152, 251, 152));
				listImgTags.setSelectionModel(new DefaultListSelectionModel() {
					private static final long serialVersionUID = 5919665928295810253L;

					@Override
				    public void setSelectionInterval(int index0, int index1) {
				        if(super.isSelectedIndex(index0)) {
				            super.removeSelectionInterval(index0, index1);
				            removeTagAndDeselect(listImgTags.getModel().getElementAt(index0));
				        } else {
				            super.addSelectionInterval(index0, index1);
				        }
				        fireValueChanged(index0, index1);
				    }
				});
				DefaultListModel<String> model = new DefaultListModel<String>();
				if(imgTags != null)
					for(String tag:imgTags)
						model.addElement(tag);
				listImgTags.setModel(model);
				JScrollPane scrollPane = new JScrollPane();
				scrollPane.setViewportView(listImgTags);
				splitPane.setLeftComponent(scrollPane);
			}
			{
				listAllTags = new JList<String>(new DefaultListModel<String>());
				listAllTags.setSelectionBackground(new Color(152, 251, 152));
				listAllTags.setSelectionModel(new DefaultListSelectionModel() {
					private static final long serialVersionUID = 7718131294733664802L;

					@Override
				    public void setSelectionInterval(int index0, int index1) {
				        if(super.isSelectedIndex(index0)) {
				            super.removeSelectionInterval(index0, index1);
				            removeTag(listAllTags.getModel().getElementAt(index0));
				        } else {
				            super.addSelectionInterval(index0, index1);
				            addTag(listAllTags.getModel().getElementAt(index0));
				        }
				        fireValueChanged(index0, index1);
				    }
				});
				orderedAllTags(); // populate list model with ordered tags
				syncLists();
				JScrollPane scrollPane = new JScrollPane();
				scrollPane.setViewportView(listAllTags);
				splitPane.setRightComponent(scrollPane);
			}
		}

		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton btnClose = new JButton("Close");
				btnClose.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
				{
					JButton btnAddNewTag = new JButton("Add new tag");
					btnAddNewTag.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							String newTag = textNewTag.getText();
							if(newTag.equals("")) return;
							if(newTag.contains(";") || newTag.contains(":")) return;
							addTag(newTag);
							
							if(getIndexOfString(listAllTags, newTag)==-1){
								((DefaultListModel<String>)listAllTags.getModel()).addElement(newTag);
								int index = getIndexOfString(listAllTags, newTag);
								if(index!=-1)
									listAllTags.setSelectionInterval(index, index);
							}
							
							textNewTag.setValue("");
								
						}
					});
					{
						JButton btnSaveMeta = new JButton("Save Meta");
						btnSaveMeta.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								if(sMeta!=null)
									sMeta.saveMeta();
							}
						});
						buttonPane.add(btnSaveMeta);
					}
					buttonPane.add(btnAddNewTag);
				}
				{

					textNewTag = new JFormattedTextField();
					textNewTag.setColumns(15);
					buttonPane.add(textNewTag);
				}
				btnClose.setActionCommand("Cancel");
				buttonPane.add(btnClose);
			}
		}
	}
	
	public void newImage(ArrayList<String> iTags, HashMap<String, Integer> aTags){
		DefaultListModel<String> model = (DefaultListModel<String>)listImgTags.getModel();
		this.imgTags = iTags;
		model.removeAllElements();
		if(this.imgTags != null){
			for(String tag:this.imgTags)
				model.addElement(tag);
		} else {
			System.out.println("INVALID imgTAGS");
			return;
		}
		this.allTags = aTags;
		orderedAllTags();
		
		syncLists();
	}
	
	private void syncLists(){
		if(imgTags!=null)
		for(String tag:imgTags){
			if(!allTags.containsKey(tag)){
				allTags.put(tag, 1);
				((DefaultListModel<String>)listAllTags.getModel()).addElement(tag);
			}
			int index = getIndexOfString(listAllTags, tag);
			listAllTags.setSelectionInterval(index, index);
		}
		listImgTags.setSelectionInterval(0, listImgTags.getModel().getSize()-1);
	}
	
	private int getIndexOfString(JList<String> jList, String item){
		ListModel<String> model = jList.getModel();
		int index = -1;
		for(int i = 0; i < model.getSize(); i++)
			if(model.getElementAt(i).equals(item)) {index = i; break;}
		return index;
	}
	
	public void noImage(){
		((DefaultListModel<String>)listImgTags.getModel()).removeAllElements();
		imgTags = null;
	}
	
	private void removeTag(String tag){
		int index = getIndexOfString(listImgTags, tag);
		if(index == -1) return;
		((DefaultListModel<String>)listImgTags.getModel()).remove(index);
		imgTags.remove(tag);
		if(allTags.containsKey(tag) && allTags.get(tag)==1) {
			System.out.println("GOT HERE");
			allTags.remove(tag);
			//((DefaultListModel<String>)listAllTags.getModel()).remove(index);
		}
		else if (allTags.containsKey(tag)) allTags.put(tag, allTags.get(tag)-1);
	}
	
	private void removeTagAndDeselect(String tag){
		removeTag(tag);
		int index = getIndexOfString(listAllTags, tag);
		if(index!=-1){
			boolean found = false;
			int[] indices = listAllTags.getSelectedIndices();
			for(int i = 0; i < indices.length; i++){
				if(indices[i] == index){found = true; break;}
			}
			if(found) listAllTags.removeSelectionInterval(index, index);
		}
	}
	
	private void addTag(String tag){
		if(getIndexOfString(listImgTags, tag)==-1){
			((DefaultListModel<String>)listImgTags.getModel()).addElement(tag);
			if(allTags.containsKey(tag)) allTags.put(tag, allTags.get(tag)+1);
			else allTags.put(tag, 1);
			imgTags.add(tag);
			int index = getIndexOfString(listImgTags, tag);
			if(index != -1)
				listImgTags.setSelectionInterval(index, index);
		}
	}
	
	private void orderedAllTags(){
		if(allTags == null) return;
		if(listAllTags == null) return;
		ArrayList<Entry<String, Integer>> arrList = new ArrayList<Entry<String, Integer>>();
		arrList.addAll(allTags.entrySet());
		arrList.sort(new Comparator<Object>(){
			@SuppressWarnings("unchecked")
			public int compare(Object o1, Object o2) {
	            return ((Entry<String,Integer>)o2).getValue().compareTo(((Entry<String,Integer>)o1).getValue());
	        }
		});

		DefaultListModel<String> model = new DefaultListModel<String>();
		listAllTags.setModel(model);
		for(Entry<String, Integer> tag:arrList)
			model.addElement(tag.getKey());
	}

}
