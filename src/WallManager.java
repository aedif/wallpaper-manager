import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.FlowLayout;
import java.awt.Image;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.Timer;
import javax.swing.border.Border;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.swing.text.NumberFormatter;
import javax.swing.JLabel;
import javax.swing.JDialog;

import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.awt.event.ActionEvent;
import javax.swing.JFormattedTextField;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class WallManager implements PathUpdate, ActionListener, SaveMeta{

	private JFrame frame;
	private JTextField textImgPath;
	private JPanel panelImages;
	private JScrollPane scrollPane;
	private StarRater starRater;
	private JButton btnSaveMeta;
	private JButton btnTags;
	private JButton btnStarX;
	private JButton btnOpenSource;
	
	private Timer recalculateTimer = new Timer( 50, this );
	
	private TagWindow tagWin;
	
	private String[] imageList;
	private int lastImage;
	private BufferedImage[] images;
	private JLabel[] lblImages;
	
	private ArrayList<String> selectedImageTags;
	private HashMap<String, Integer> allImageTags;
	
	private WallpaperPath wallpaperPath;
	
	private WallManager wManager;
	
	private int selectedImage = -1;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					WallManager window = new WallManager();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public WallManager() {
		initialize();
		wManager = this;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		wallpaperPath = new WallpaperPath(this);
		recalculateTimer.stop();
		
		frame = new JFrame();
		frame.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				if ( recalculateTimer.isRunning() ){
					recalculateTimer.restart();
				} else {
					recalculateTimer.start();
				}
			}
		});
		frame.setBounds(100, 100, 600, 166);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.NORTH);
		panel.setLayout(new FlowLayout(FlowLayout.LEFT, 1, 0));
		
		JButton btnStart = new JButton("Start");
		panel.add(btnStart);
		
		JButton btnStop = new JButton("Stop");
		btnStop.setEnabled(false);
		panel.add(btnStop);
		
		JLabel lblOfPic = new JLabel("# of pic");
		panel.add(lblOfPic);
		
		NumberFormat format = NumberFormat.getInstance();
	    NumberFormatter formatter = new NumberFormatter(format);
	    formatter.setValueClass(Integer.class);
	    formatter.setMinimum(1);
	    formatter.setMaximum(86400);
	    formatter.setAllowsInvalid(false);
		
		JFormattedTextField fTextNumOfPic = new JFormattedTextField(formatter);
		fTextNumOfPic.setColumns(4);
		fTextNumOfPic.setValue(5);
		panel.add(fTextNumOfPic);
		
		JLabel lblUpdF = new JLabel("upd f");
		panel.add(lblUpdF);
		
		JFormattedTextField fTextUpdateFrequency = new JFormattedTextField(formatter);
		fTextUpdateFrequency.setColumns(4);
		fTextUpdateFrequency.setValue(10);
		panel.add(fTextUpdateFrequency);
		
		JLabel label = new JLabel("||");
		panel.add(label);
		
		btnSaveMeta = new JButton("Save meta");
		btnSaveMeta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				saveMeta();
			}
		});
		panel.add(btnSaveMeta);
		
		scrollPane = new JScrollPane( 
				JScrollPane.VERTICAL_SCROLLBAR_NEVER,
	            JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		frame.getContentPane().add(scrollPane, BorderLayout.CENTER);
		
		panelImages = new JPanel();
		scrollPane.setViewportView(panelImages);
		panelImages.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
        
        starRater = new StarRater(5, 0, 0);
        starRater.setRating(0.0f);
        panel.add(starRater);
        
        btnStarX = new JButton("x");
        btnStarX.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		starRater.setRating(0.0f);
        		starRater.setSelection(-1);
        	}
        });
        panel.add(btnStarX);
        
        btnTags = new JButton("Tags");
        btnTags.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		if(tagWin!= null && tagWin.isShowing()){
        			tagWin.toFront();
        			return;
        		}
        		if(tagWin != null){
        			tagWin.setVisible(true);
        			if(selectedImage == -1) tagWin.noImage();
        			else {
        				allImageTags = TagEditor.readTagsFromFile();
        				tagWin.newImage(selectedImageTags, allImageTags);
        			}
        			return;
        		}

        		if(selectedImage != -1){
        			allImageTags = TagEditor.readTagsFromFile();
        			tagWin = new TagWindow(selectedImageTags, allImageTags, wManager);
        		} else {
        			allImageTags = TagEditor.readTagsFromFile();
        			tagWin = new TagWindow(null, allImageTags, wManager);
        		}
        		tagWin.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        		tagWin.setVisible(true);
        	}
        });
        panel.add(btnTags);
        
        JPanel panel_2 = new JPanel();
        frame.getContentPane().add(panel_2, BorderLayout.SOUTH);
        panel_2.setLayout(new BorderLayout(0, 0));
        
        btnOpenSource = new JButton("Open Source");
        btnOpenSource.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		try{
        			Runtime.getRuntime().exec("explorer.exe /select," + imageList[selectedImage]);
        		} catch (IOException ea){}
        	}
        });
        panel_2.add(btnOpenSource, BorderLayout.WEST);
        
        textImgPath = new JTextField();
        panel_2.add(textImgPath, BorderLayout.CENTER);
        textImgPath.setColumns(10);
        
        // DISABLE IMAGE SPECIFIC CONTROLS
        disableImageControls();
        
		// ACTION LISTENERS
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				removeLabels();
				images = new BufferedImage[(int)fTextNumOfPic.getValue()];
				imageList = new String[(int)fTextNumOfPic.getValue()];
				lblImages = new JLabel[(int)fTextNumOfPic.getValue()];
				lastImage = 0;
				wallpaperPath.setUpdateFrequency(
						((int)fTextUpdateFrequency.getValue()*1000));
				wallpaperPath.startAutoUpdate();
				btnStart.setEnabled(false);
				btnStop.setEnabled(true);
				fTextUpdateFrequency.setEnabled(false);
				fTextNumOfPic.setEnabled(false);
			}
		});
		
		btnStop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				wallpaperPath.stopAutoUpdate();
				btnStop.setEnabled(false);
				btnStart.setEnabled(true);
				fTextUpdateFrequency.setEnabled(true);
				fTextNumOfPic.setEnabled(true);
			}
		});
		
        starRater.addStarListener(
                new StarRater.StarListener(){

                    public void handleSelection(int selection) {
                        System.out.println(selection);
                        starRater.setRating(selection);
                    }
        });
	}

	@Override
	public void newPath(String imgPath) {
		//textImgPath.setText(imgPath); //temp
		updateList(imgPath);
	}
	
	private void removeLabels(){
		if(panelImages == null) return;
		if(lblImages == null) return;
		for(int i = 0; i < lblImages.length; i++){
			if(lblImages[i]!=null)
				panelImages.remove(lblImages[i]);
		}
		panelImages.revalidate();
	}
	
	private void updateList(String imgPath){
		if(imgPath == null) return;
		if(lastImage == selectedImage){
			//System.out.println("lastImage: " + lastImage);
			//System.out.println("selectedImage: " + selectedImage);
			if(images.length <= 1) return;
			lastImage = (++lastImage) % imageList.length;
		}
		imageList[lastImage] = imgPath;
		
		try {
			images[lastImage] = ImageIO.read(new File(imgPath));
		} catch (IOException e) {
			try {
				images[lastImage] = ImageIO.read(new File(System.getProperty("user.dir") + "/data/noImage.png"));
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		    e.printStackTrace();
		}
		
		updateImgLabels();

		lastImage++;
		lastImage = lastImage % imageList.length;
		
		//temp
		String temp = "";
		for(int i = 0; i < imageList.length; i++){
			temp += imageList[i] + "\n";
		}
		System.out.println(temp);
	}
	
	private void updateImgLabels(){
		if(lblImages[lastImage]!=null)
			panelImages.remove(lblImages[lastImage]);
		JLabel label = new JLabel(resizeImage(images[lastImage]));
		label.addMouseListener(new MouseAdapter(){  
			int n = lastImage;
		    public void mouseClicked(MouseEvent e){  
		    	labelSelected(n);
		    }  
		});
		lblImages[lastImage] = label;
		panelImages.add(lblImages[lastImage], 0);
		
		if(selectedImage != -1){
			scrollPane.validate();
			panelImages.scrollRectToVisible(lblImages[selectedImage].getBounds());
		}
		
		scrollPane.revalidate();
	}
	
	private void resizeUpdate(){
		System.out.println("Resizing");
		if(lblImages == null) return;
		for(int i = 0; i<lblImages.length; i++){
			if(lblImages[i]!=null && images[i]!=null){
				lblImages[i].setIcon(resizeImage(images[i]));
			}
		}
	}
	
	private ImageIcon resizeImage(BufferedImage img){
		float width = (float)img.getWidth()*((float)(scrollPane.getHeight()-30)/((float)img.getHeight()));
		int height = scrollPane.getHeight()-30;
		if(width <= 5 || height<= 5)
			return new ImageIcon(img.getScaledInstance(10, 10, Image.SCALE_SMOOTH));
		return new ImageIcon(img.getScaledInstance((int)width, height, Image.SCALE_SMOOTH));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		recalculateTimer.stop();
		resizeUpdate();
	}
	
	private void labelSelected(int n){
		//remove border from previous selected
		if(selectedImage != -1)
			if(lblImages[selectedImage] != null) lblImages[selectedImage].setBorder(null);
		
		if(selectedImage == n){
			lblImages[selectedImage].setBorder(null);
			selectedImage = -1;
			textImgPath.setText("No image selected.");
			selectedImageTags = null;
			disableImageControls();
			if(tagWin != null)
				tagWin.noImage();
			return;
		}
		
		selectedImage = n;
		
		// IMAGE/LBL SELECTED
		btnOpenSource.setEnabled(true);
		getImageTagsAndRating();
		enableImageControls();
		
		// SET BORDER
		Border compound;
		Border redline = BorderFactory.createLineBorder(Color.red);
		compound = BorderFactory.createCompoundBorder(
				BorderFactory.createRaisedBevelBorder(), BorderFactory.createLoweredBevelBorder());
		lblImages[n].setBorder(BorderFactory.createCompoundBorder(compound, redline));
		
		// SET focus on selected image
		scrollPane.validate();
		panelImages.scrollRectToVisible(lblImages[selectedImage].getBounds());
		scrollPane.revalidate();
		
		// Display info
		textImgPath.setText(imageList[n]);
		
		//temp test
		//TagEditor.checkSegments(imageList[selectedImage]);
	}
	
	private void disableImageControls(){
        // DISABLE IMAGE SPECIFIC CONTROLS
        starRater.setEnabled(false);
        starRater.setRating(0.0f);
        starRater.setSelection(0);
        btnSaveMeta.setEnabled(false);
        btnTags.setEnabled(false);
        btnStarX.setEnabled(false);
        btnOpenSource.setEnabled(false);
	}
	
	private void enableImageControls(){
        // ENABLE IMAGE SPECIFIC CONTROLS
        starRater.setEnabled(true);
        btnSaveMeta.setEnabled(true);
        btnTags.setEnabled(true);
        btnStarX.setEnabled(true);
        btnOpenSource.setEnabled(true);
	}
	
	private void getImageTagsAndRating(){
		if (selectedImage != -1){
			selectedImageTags = TagEditor.getImageTags(imageList[selectedImage]);
		} else {
			return;
		}
		
		Iterator<String> iterator = selectedImageTags.iterator();
		while(iterator.hasNext()){
			String tag = iterator.next();
			if(tag.startsWith("RATING:")){
				starRater.setRating((float)Character.getNumericValue(tag.charAt(tag.length()-1)));
				//starRater.setSelection(0);
				iterator.remove();
			}
		}
		
		if(tagWin != null){
			allImageTags = TagEditor.readTagsFromFile();
			tagWin.newImage(selectedImageTags, allImageTags);
		}
	}
	
	public void saveMeta(){
		TagEditor.writeImageTags(imageList[selectedImage], selectedImageTags, starRater.getSelection());
		TagEditor.writeTagsToFile(allImageTags);
	}
}
