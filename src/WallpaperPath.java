import com.sun.jna.platform.win32.Advapi32Util;
import com.sun.jna.platform.win32.WinReg;

public class WallpaperPath implements Runnable{

	private long updateFrequency;
	private PathUpdate toUpdate;
	private volatile boolean running = false;
	private Thread thread;

	public WallpaperPath(PathUpdate toUpdate){
		this(toUpdate, 10000);
	}

	public WallpaperPath(PathUpdate toUpdate, long updateFrequency){
		this.toUpdate = toUpdate;
		this.updateFrequency = updateFrequency;
	}

	// gets the path of the current desktop wallpaper
	public String getPath(){
        byte[] pEncoding = Advapi32Util.registryGetBinaryValue(
            WinReg.HKEY_CURRENT_USER, "Control Panel\\Desktop", "TranscodedImageCache");
        
        // if array returned is empty, return null
        if(pEncoding.length <= 24) return null;

        byte[] path = new byte[pEncoding.length - 24];

        for(int i = 24; i<pEncoding.length; i+=2)
        	path[(i-24)/2] = pEncoding[i];
        return new String(path).trim();
	}
	
	public void startAutoUpdate(){
		if(thread != null)
			thread.interrupt();
		running = true;
		thread = new Thread(this);
		thread.start();
	}
	
	public void stopAutoUpdate(){
		if(thread != null)
			thread.interrupt();
		running = false;
	}
	
	public void setUpdateFrequency(long updateFrequency){
		this.updateFrequency = updateFrequency;
	}

	@Override
	public void run() {
		while(running){
			toUpdate.newPath(getPath());
			try {
				Thread.sleep(updateFrequency);
			} catch (InterruptedException e) {
				break;
			}
		}
	}

}