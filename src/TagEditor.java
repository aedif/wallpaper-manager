import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.imaging.ImageFormats;
import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.ImageWriteException;
import org.apache.commons.imaging.Imaging;
import org.apache.commons.imaging.ImagingConstants;
import org.apache.commons.imaging.common.ImageMetadata;
import org.apache.commons.imaging.common.ImageMetadata.ImageMetadataItem;
import org.apache.commons.imaging.common.bytesource.ByteSource;
import org.apache.commons.imaging.common.bytesource.ByteSourceFile;
import org.apache.commons.imaging.formats.jpeg.JpegImageMetadata;
import org.apache.commons.imaging.formats.jpeg.JpegImageParser;
import org.apache.commons.imaging.formats.jpeg.JpegPhotoshopMetadata;
import org.apache.commons.imaging.formats.jpeg.exif.ExifRewriter;
import org.apache.commons.imaging.formats.jpeg.iptc.IptcBlock;
import org.apache.commons.imaging.formats.jpeg.iptc.IptcRecord;
import org.apache.commons.imaging.formats.jpeg.iptc.IptcTypes;
import org.apache.commons.imaging.formats.jpeg.iptc.JpegIptcRewriter;
import org.apache.commons.imaging.formats.jpeg.iptc.PhotoshopApp13Data;
import org.apache.commons.imaging.formats.jpeg.xmp.JpegXmpRewriter;
import org.apache.commons.imaging.formats.png.PngImageParser;
import org.apache.commons.imaging.formats.tiff.TiffImageMetadata;
import org.apache.commons.imaging.formats.tiff.constants.MicrosoftTagConstants;
import org.apache.commons.imaging.formats.tiff.fieldtypes.FieldType;
import org.apache.commons.imaging.formats.tiff.taginfos.TagInfoXpString;
import org.apache.commons.imaging.formats.tiff.write.TiffOutputDirectory;
import org.apache.commons.imaging.formats.tiff.write.TiffOutputField;
import org.apache.commons.imaging.formats.tiff.write.TiffOutputSet;
import org.apache.commons.imaging.util.IoUtils;

public class TagEditor {
	
	private static String tagFile = System.getProperty("user.dir") + "/data/tags.txt";
	
	public static HashMap<String, Integer> readTagsFromFile(){
		
		HashMap<String, Integer> tags = new HashMap<String, Integer>();

        try {
        	FileReader input = new FileReader(tagFile);
        	BufferedReader bufRead = new BufferedReader(input);
        	String line = null;
        	bufRead.readLine();
        	while ((line = bufRead.readLine()) != null){
        		String[] split = line.split(":");
        		//if(!split[0].equals(""))
        		tags.put(split[0], Integer.parseInt(split[1]));
        	}
			bufRead.close();
		} catch (IOException e) {
			System.out.println("Unable to read tag file.");
			return null;
		}
		
		return tags;
	}
	
	public static boolean writeTagsToFile(HashMap<String, Integer> tags){
		if(tags == null) return false;
		FileWriter output;
		try {
			output = new FileWriter(tagFile);
			BufferedWriter bufWrite = new BufferedWriter(output);
			bufWrite.write("[Tag] [# of uses]");
			System.out.println("tags size = " + tags.size());
			System.out.println("[Tag] [# of uses]");
			for(String tag: tags.keySet()){
				System.out.println(tag + ":" + tags.get(tag));
				bufWrite.write("\n"+ tag + ":" + tags.get(tag));
			}
			bufWrite.close();
		} catch (IOException e) {
			System.out.println("Unable to write tags to file");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public static ArrayList<String> getImageTags(String imgPath){
		File imgFile = new File(imgPath);
		if(Imaging.hasImageFileExtension(imgFile)){
			String name = imgFile.getName();
			String extension = name.substring(name.lastIndexOf('.'));
			if(extension.equalsIgnoreCase(".jpg") || extension.equalsIgnoreCase(".jpeg"))
				return getJpegImageTags(imgPath);
			else if (extension.equalsIgnoreCase(".png"))
				return getPngImageTags(imgPath);
			else{
				System.out.println("Not a supported img format: " + extension);
				return new ArrayList<String>();
			}
		} else {
			System.out.println("Cannot recognise the file as an Image.");
			return new ArrayList<String>();
		}
	}
	
	// rating complete
	private static ArrayList<String> getPngImageTags(String imgPath){
		HashSet<String> tagSet = new HashSet<String>();
		File imgFile = new File(imgPath);
		try {
			String xmp = new PngImageParser().getXmpXml(new ByteSourceFile(imgFile), new HashMap<String, Object>());
			if(xmp == null) return new ArrayList<String>(tagSet);
			int tagRDF = xmp.indexOf("<rdf:Description rdf:about=\"WallpaperManager_Image_Tags\">");
			int tagRDFend = xmp.indexOf("</rdf:Description>", tagRDF);
			if(tagRDF == -1)
				return new ArrayList<String>();
			
			int tagLine = xmp.indexOf("<rdf:li>");
			while(tagLine != -1 && tagLine < tagRDFend){
				System.out.println("here");
				int tagLineEnd = xmp.indexOf("</rdf:li>", tagLine);
				if(tagLineEnd > tagRDFend) break;
				if(tagLineEnd == -1) break;
				tagSet.add(xmp.substring(tagLine+"<rdf:li>".length(), tagLineEnd));
				tagLine = xmp.indexOf("<rdf:li>", tagLineEnd);
			}
		} catch (ImageReadException | IOException e) {
			e.printStackTrace();
		}
		return new ArrayList<String>(tagSet);
	}
	
	//rating complete
	private static ArrayList<String> getJpegImageTags(String imgPath){
		HashSet<String> tagSet = new HashSet<String>();
		File imgFile = new File(imgPath);
		
		try {
			ImageMetadata metadata = Imaging.getMetadata(imgFile);

			if(metadata != null)
				System.out.println("Format: " + metadata.getClass());
			
			if (metadata instanceof JpegImageMetadata){
				JpegImageMetadata jpegMetadata = (JpegImageMetadata) metadata;
				List<ImageMetadataItem> metaItems = jpegMetadata.getItems();
				for(ImageMetadataItem item: metaItems){
					String itemString = item.toString();
					System.out.println(itemString);
					if(itemString.startsWith("Keywords: ")){
						//tagSet.add(itemString.substring(itemString.lastIndexOf("Keywords: ")));
						tagSet.add(itemString.replace("Keywords: ", ""));
					} else if(itemString.startsWith("XPKeywords: ")){
						String[] itemTags = itemString.replaceAll("'|XPKeywords: ","").split(";");
						for(int i=0; i<itemTags.length; i++){
							if(!itemTags[i].equals(""))
								tagSet.add(itemTags[i]);
						}
					} else if(itemString.startsWith("Rating: ")){
						tagSet.add("RATING:" + itemString.charAt("Rating: ".length()));
					}
				}
			} else {
				
				String xmpXML = Imaging.getXmpXml(imgFile);
				if(metadata != null)
					System.out.println(metadata.toString());
				if(xmpXML != null)
					System.out.print(xmpXML);
			}
		} catch (ImageReadException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		return new ArrayList<String>(tagSet);
	}
	
	public static void writeImageTags(String imgPath, ArrayList<String> tags, int rating){
		File imgFile = new File(imgPath);
		if(Imaging.hasImageFileExtension(imgFile)){
			String name = imgFile.getName();
			String extension = name.substring(name.lastIndexOf('.'));
			if(extension.equalsIgnoreCase(".jpg") || extension.equalsIgnoreCase(".jpeg"))
				jpegWriteTags(imgPath, tags, rating);
			else if (extension.equalsIgnoreCase(".png"))
				pngWriteTags(imgPath, tags, rating);
			else
				System.out.println("Not a supported img format: " + extension);
		} else {
			System.out.println("Cannot recognise the file as an Image.");
		}
	}
	
	//rating complete
	private static void pngWriteTags(String imgPath, ArrayList<String> tags, int rating){
		
		final String newXmpStart = "<?xpacket begin=\"?\" id=\"W5M0MpCehiHzreSzNTczkc9d\"?>"
				+ "<x:xmpmeta xmlns:x=\"adobe:ns:meta/\">"
				+ "<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">";
		final String newXmpEnd = "</rdf:RDF>"
				+ "</x:xmpmeta>"
				+ "<?xpacket end=\"w\"?>";
		final String xmpTagStart = "<rdf:Description rdf:about=\"WallpaperManager_Image_Tags\">"
				+ "<dc:subject>"
				+ "<rdf:Bag>";
		final String xmpTagEnd = "</rdf:Bag>" + "</dc:subject>" + "</rdf:Description>";
		
		// temp
		System.out.println("INSIDE OF pngWriteTags()");
//		tags = new ArrayList<String>();
//		tags.add("random");
//		tags.add("random2");
//		tags.add("RATING:" + Integer.toString(rating));
		//---
		
		
		removePngXmpTags(imgPath);
		if (tags == null || tags.size() == 0)
			return;
		
		String tagString = "";
		for(String tag:tags){
			tagString += "<rdf:li>" + tag + "</rdf:li>";
		}
		
		if(rating!=-1)
			tagString += "<rdf:li>RATING:" + rating + "</rdf:li>";
		
		File imgFile = new File(imgPath);
		try {
			String xmp = new PngImageParser().getXmpXml(new ByteSourceFile(imgFile), new HashMap<String, Object>());
			
			if(xmp == null){
				xmp = newXmpStart + xmpTagStart + tagString + xmpTagEnd + newXmpEnd;
			} else {;
				String insert = "";
				String find = "<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">";
				int insertPoint = xmp.indexOf(find)+find.length();
				if(insertPoint > find.length()){
					System.out.println("herehello");
					insert = xmpTagStart + tagString + xmpTagEnd;
				} else {
					find = "<x:xmpmeta xmlns:x=\"adobe:ns:meta/\">";
					insertPoint = xmp.indexOf(find) + find.length();
					if(insertPoint > find.length()){
						insert = "<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">"
								+ xmpTagStart + tagString + xmpTagEnd + "</rdf:RDF>";
					} else {
						find = "<?xpacket begin=\"?\" id=\"W5M0MpCehiHzreSzNTczkc9d\"?>";
						String find2 = "<?xpacket begin=\"?\" id=\"W5M0MpCehiHzreSzNTczkc9d\"?>";
						insertPoint = xmp.indexOf(find)+find.length();
						if (insertPoint > find.length()){
							insert = "<x:xmpmeta xmlns:x=\"adobe:ns:meta/\">"
									+ "<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">"
									+ xmpTagStart + tagString + xmpTagEnd + "</rdf:RDF>" + "</x:xmpmeta>";
						} else if ((insertPoint = xmp.indexOf(find2)+find2.length())>find2.length()){
							System.out.println("here4");
							insert = "<x:xmpmeta xmlns:x=\"adobe:ns:meta/\">"
									+ "<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">"
									+ xmpTagStart + tagString + xmpTagEnd + "</rdf:RDF>" + "</x:xmpmeta>";
						} else {
							xmp = "";
							insert = newXmpStart + xmpTagStart + tagString + xmpTagEnd + newXmpEnd;
							insertPoint = 0;
						}
					}
				}
				
				if(insertPoint == 0){
					xmp = insert;
				}else{
					xmp = xmp.substring(0, insertPoint) + insert + xmp.substring(insertPoint);
				}
			}
			
			System.out.println("XMP TO WRITE:\n" + xmp);
			//write image
			Map<String, Object> params = new HashMap<String, Object>();
			params.put(ImagingConstants.PARAM_KEY_XMP_XML, xmp);
			String outPath = imgPath.substring(0, imgPath.toLowerCase().lastIndexOf(".png")) + "_temp.png";
			File outFile = new File(outPath);
			Imaging.writeImage(Imaging.getBufferedImage(imgFile), outFile, ImageFormats.PNG, params);
			
			// remove ori, rename temp
    		imgFile.delete();
    		outFile.renameTo(new File(imgPath));
		} catch (ImageReadException | IOException e) {
			e.printStackTrace();
		} catch (ImageWriteException e) {
			e.printStackTrace();
		}
	}
	
	// rating complete
	private static void removePngXmpTags(String imgPath){
		File imgFile = new File(imgPath);
		try {
			String xmp = new PngImageParser().getXmpXml(new ByteSourceFile(imgFile), new HashMap<String, Object>());
			if(xmp == null)
				return;
			
			String findStart = "<rdf:Description rdf:about=\"WallpaperManager_Image_Tags\">";
			String findEnd = "</rdf:Description>";
			int removeStart = xmp.indexOf(findStart);
			if(removeStart == -1)
				return;
			int removeEnd = xmp.indexOf(findEnd, removeStart);
			if(removeEnd==-1){
				System.out.println("Invalid xmp.");
				return;
			}
			
			System.out.println("XMP BEFORE TAG REMOVE: \n" + xmp);
			xmp = xmp.substring(0, removeStart) + xmp.substring(removeEnd + findEnd.length());
			System.out.println("XMP AFTER TAG REMOVE: \n" + xmp);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put(ImagingConstants.PARAM_KEY_XMP_XML, xmp);
			String outPath = imgPath.substring(0, imgPath.toLowerCase().lastIndexOf(".png")) + "_temp.png";
			File outFile = new File(outPath);
			Imaging.writeImage(Imaging.getBufferedImage(imgFile), outFile, ImageFormats.PNG, params);
			
			// rmeove ori, rename temp
    		imgFile.delete();
    		outFile.renameTo(new File(imgPath));
		} catch (ImageReadException | IOException e) {
			e.printStackTrace();
		} catch (ImageWriteException e) {
			e.printStackTrace();
		}
	}
	
	// need to write XMP rating
	private static void jpegWriteTags(String imgPath, ArrayList<String> tags, int rating){
		
		JpegImageParser jParser = new JpegImageParser();
		try {
			if(jParser.hasXmpSegment(new ByteSourceFile(new File(imgPath))))
				removeJpegXmpTags(imgPath);
			if(jParser.hasIptcSegment(new ByteSourceFile(new File(imgPath))))
				removeJpegIptcTags(imgPath);
		} catch (ImageReadException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		// temp
//		tags = new ArrayList<String>();
//		tags.add("random");
//		tags.add("random2");
		
		String tagString = "";
		for(String tag: tags)
			tagString += tag + ";";
		//---
		
		File imgFile = new File(imgPath);
		File outImgFile;
		
		if(imgFile.getName().endsWith(".jpg"))
			outImgFile = new File(imgPath.replace(".jpg", "_temp.jpg"));
		else if(imgFile.getName().endsWith(".jpeg")){
			outImgFile = new File(imgPath.replace(".jpeg", "_temp.jpeg"));
		} else {
			System.out.println("jpegWriteTags(): unable to identify img extension");
			return;
		}
		
		ImageMetadata metadata;
		OutputStream os = null;
		
		try {
			
			TiffOutputSet outputSet = null;
			metadata = Imaging.getMetadata(imgFile);
			TiffImageMetadata exif;
			
		    if (metadata instanceof JpegImageMetadata) {
		        exif = ((JpegImageMetadata) metadata).getExif();
		    } else if (metadata instanceof TiffImageMetadata) {
		        exif = (TiffImageMetadata)metadata;
		    } else {
		    	System.out.println("Metadata didnt match either type");
		    	exif = null;
		    }
		    
            if (null != metadata) {
                if (null != exif) {
                    outputSet = exif.getOutputSet();
                }
            }
            
            if (null == outputSet) {
                outputSet = new TiffOutputSet();
            }
            
            TiffOutputDirectory exifDirectory = outputSet.getOrCreateRootDirectory();
            
            TiffOutputField xpkeywords = exifDirectory
            	    .findField(MicrosoftTagConstants.EXIF_TAG_XPKEYWORDS);
            if (xpkeywords != null) {
            	exifDirectory.removeField(MicrosoftTagConstants.EXIF_TAG_XPKEYWORDS);
            }
            
            //remove rating
            if (exifDirectory
            	    .findField(MicrosoftTagConstants.EXIF_TAG_RATING) != null) {
            	System.out.println("TAG RATING REMOVED|RATING TO ADD:" + rating);
            	exifDirectory.removeField(MicrosoftTagConstants.EXIF_TAG_RATING);
            }
            
            if (exifDirectory
            	    .findField(MicrosoftTagConstants.EXIF_TAG_RATING_PERCENT) != null) {
            	System.out.println("TAG PERCENT REMOVED");
            	exifDirectory.removeField(MicrosoftTagConstants.EXIF_TAG_RATING_PERCENT);
            }
            
            //add rating
            if(rating != -1){

               exifDirectory.add(MicrosoftTagConstants.EXIF_TAG_RATING, (short)rating);

               short percent = 1;
               switch(rating){
               	case 1: percent = 1;  break;
               	case 2: percent = 25; break;
               	case 3: percent = 50; break;
               	case 4: percent = 75; break;
               	case 5: percent = 99; break;
               }

               exifDirectory.add(MicrosoftTagConstants.EXIF_TAG_RATING_PERCENT, percent);
            }
            
            TagInfoXpString xpString = MicrosoftTagConstants.EXIF_TAG_XPKEYWORDS;
            byte[] stringToWrite = xpString.encodeValue(FieldType.BYTE, tagString, ByteOrder.nativeOrder());

            TiffOutputField tiffOutputField = new TiffOutputField(
            		MicrosoftTagConstants.EXIF_TAG_XPKEYWORDS,
            		FieldType.BYTE,
            	    stringToWrite.length, stringToWrite);
            

            exifDirectory.add(tiffOutputField);
            
            os = new FileOutputStream(outImgFile);
            os = new BufferedOutputStream(os);
            
            new ExifRewriter().updateExifMetadataLossless(imgFile, os, outputSet);
            
            // Removed original file, and renames the output to the original
    		imgFile.delete();
    		outImgFile.renameTo(new File(imgPath));

		} catch (ImageReadException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ImageWriteException e) {
			e.printStackTrace();
		} finally {
			try {
				IoUtils.closeQuietly(true, os);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public static void checkSegments(String imgPath){
		File imgFile = new File(imgPath);

		ByteSource byteSource = new ByteSourceFile(imgFile);
		try {
			JpegImageParser parser = new JpegImageParser();
			if(parser.hasIptcSegment(byteSource)){
				System.out.println("has IPTC segment");
			}
			if(parser.hasExifSegment(byteSource))
				System.out.println("has EXIF segment");
			if(parser.hasXmpSegment(byteSource))
				System.out.println("has XMP segment");
		} catch (ImageReadException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	// rating complete
	private static void removeJpegXmpTags(String imgPath){
		File imgFile = new File(imgPath);
		OutputStream os = null;
		try {
			String xmp = Imaging.getXmpXml(imgFile);
			String outXmp = "";
			
			//rem trash before the actual tags...
			int remStart = xmp.lastIndexOf("<dc:subject><rdf:Bag xmlns:rdf=", xmp.indexOf("<MicrosoftPhoto:LastKeywordXMP>"));
			int remEnd = xmp.lastIndexOf("</dc:subject>", xmp.indexOf("<MicrosoftPhoto:LastKeywordXMP>"))+13;
			
			if(remStart != -1 && remEnd != -1)
				outXmp = xmp.substring(0, remStart) + xmp.substring(remEnd, xmp.length());
			else
				outXmp = xmp;
			
			remStart = outXmp.indexOf("<MicrosoftPhoto:LastKeywordXMP>");
			remEnd = outXmp.indexOf("</MicrosoftPhoto:LastKeywordXMP>")+32;
			
			if(remStart != -1 && remEnd != -1)
				outXmp = outXmp.substring(0, remStart) + outXmp.substring(remEnd, outXmp.length());
			
			remStart = outXmp.indexOf("<MicrosoftPhoto:LastKeywordIPTC>");
			remEnd = outXmp.indexOf("</MicrosoftPhoto:LastKeywordIPTC>")+33;
			if(remStart != -1 && remEnd != -1)
				outXmp = outXmp.substring(0, remStart) + outXmp.substring(remEnd, outXmp.length());
			
			//remove rating
			String find = "</MicrosoftPhoto:Rating>";
			remStart = outXmp.indexOf("<MicrosoftPhoto:Rating>");
			remEnd = outXmp.indexOf(find);
			if(remStart != -1 && remEnd != -1)
				outXmp = outXmp.substring(0, remStart) + outXmp.substring(remEnd+find.length());
			
			find = "</xap:Rating>";
			remStart = outXmp.lastIndexOf("<xap:Rating>", remStart);
			remEnd = outXmp.lastIndexOf(find, remEnd);
			if(remStart != -1 && remEnd !=-1)
				outXmp = outXmp.substring(0, remStart) + outXmp.substring(remEnd+find.length());
			//---
			
			System.out.println(xmp);
			System.out.println(outXmp);
			
			//updating xmp
			File outImgFile;
			
			if(imgFile.getName().endsWith(".jpg"))
				outImgFile = new File(imgPath.replace(".jpg", "_temp.jpg"));
			else if(imgFile.getName().endsWith(".jpeg")){
				outImgFile = new File(imgPath.replace(".jpeg", "_temp.jpeg"));
			} else {
				System.out.println("removeJpegXmpTags(): unable to identify img extension");
				return;
			}
				
			os = new FileOutputStream(outImgFile);
            os = new BufferedOutputStream(os);
            
			new JpegXmpRewriter().updateXmpXml(imgFile, os, outXmp);
			
			//remove original, rename temp
    		imgFile.delete();
    		outImgFile.renameTo(new File(imgPath));
			
		} catch (ImageReadException | IOException e) {
			e.printStackTrace();
		} catch (ImageWriteException e) {
			e.printStackTrace();
		} finally{
			try {
				if(os!=null){
					os.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	// probably does not contain rating
	private static void removeJpegIptcTags(String imgPath){
		File imgFile = new File(imgPath);

		ByteSource byteSource = new ByteSourceFile(imgFile);
		
        OutputStream os = null;

		try {
			JpegPhotoshopMetadata metadata = new JpegImageParser()
					.getPhotoshopMetadata(byteSource, null);
			List<IptcRecord> iptcRecords = metadata.photoshopApp13Data.getRecords();
			List<IptcBlock> nonIptcBlocks = metadata.photoshopApp13Data.getNonIptcBlocks();
			
			Iterator<IptcRecord> recordIterator = iptcRecords.iterator();
			while(recordIterator.hasNext()){
				IptcRecord record = recordIterator.next();
				if(record.iptcType == IptcTypes.KEYWORDS){
					recordIterator.remove();
					System.out.println("REMOVED{= " + record.getIptcTypeName() + ": " + record.getValue());
				}
				//if(record.iptcType == IptcTypes.)
			}
			
			PhotoshopApp13Data newData = new PhotoshopApp13Data(iptcRecords, nonIptcBlocks);
			
            imgFile = new File(imgPath);
            File outImgFile;
			if(imgFile.getName().endsWith(".jpg"))
				outImgFile = new File(imgPath.replace(".jpg", "_temp.jpg"));
			else if(imgFile.getName().endsWith(".jpeg")){
				outImgFile = new File(imgPath.replace(".jpeg", "_temp.jpeg"));
			} else {
				System.out.println("removeJpegIptcTags(): unable to identify img extension");
				return;
			}
			
			os = new FileOutputStream(outImgFile);
            os = new BufferedOutputStream(os);
            new JpegIptcRewriter().writeIPTC(byteSource, os, newData);
            
			//remove original, rename temp
    		imgFile.delete();
    		outImgFile.renameTo(new File(imgPath));
		} catch (ImageReadException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ImageWriteException e) {
			e.printStackTrace();
		} finally{
			try {
				os.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
